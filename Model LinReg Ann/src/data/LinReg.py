import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

def model(X_train, y_train, X_test, y_test, X_full):
	print("Runing LinReg model")
	#	Train model
	model = LinearRegression(fit_intercept=True, normalize=False, copy_X=True, n_jobs=None).fit(X_train, y_train)
#	score_train = model.score(X_train, y_train) # Uncomment to get score reached in training
	#   Test model
#	score_test = model.score(X_test, y_test) # Uncomment to get score of test data on the trained model
	#	Predict
#	y_prediction_test = model.predict(X_test) # Uncomment to get prediction on test data
#	y_prediction_train = model.predict(X_train) # Uncomment to get orediction on train data
#	error = abs(y_prediction_test-y_test) # Uncomment to get absolut error on test data
#	msq = mean_squared_error(y_prediction_test,y_test) # Uncomment to get MSQ on test data
	y_prediction = model.predict(X_full) # Prediction based on the full timeseries data
		
	return y_prediction