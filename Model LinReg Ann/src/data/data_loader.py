#!/usr/bin/python

import pandas as pd
from tqdm import tqdm
import numpy as np
from array import *
from keras.preprocessing.sequence import TimeseriesGenerator


source_recoverd = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv"
source_dead = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv"
source_infected = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv"

def fetch_data_jhu(link):
	data = pd.read_csv(link)
	# for countries like australia or canada, wher JHU only provides numbers on regional level
	data = data.groupby(['Country/Region']).sum().reset_index()
	# drop unneccessary cols 
	data = data.drop(["Lat","Long"], axis=1)
	return data	
	
def generate_data(data, country, test_train_split, days_predicted, state):
	x_1_raw = data[0]
	x_2_raw = data[1]
	x_3_raw = data[2]
	y_raw = data[2]
	x_1 = []
	x_2 = []
	x_3 = []
	y = []
	length_raw = len(x_1_raw)
	# Truncate days at the start with no infections
	val = next((i for i, x in enumerate(data[state]) if x != 0), None)
	for i in range(length_raw-val):
		x_1.insert(i,x_1_raw[val+i-days_predicted])
		x_2.insert(i,x_2_raw[val+i-days_predicted])
		x_3.insert(i,x_3_raw[val+i-days_predicted])
		y.insert(i,y_raw[val+i])
	# Truncated data set
	length = len(x_1)
	X_full = [] # Store the truncated dataset of R,I,D
	for i in range(length):
		add_x = [x_1_raw[i+val],x_2_raw[i+val],x_3_raw[i+val]]
		X_full.insert(i,add_x)
		
	train_split_country = int(length*test_train_split)	
	# generate training data
	X_train = []
	y_train = []
	for i in range(train_split_country):
		add_x = [x_1[i],x_2[i],x_3[i]]
		add_y = y[i]
		X_train.insert(i,add_x)
		y_train.insert(i,add_y)	
	# generate test data
	X_test = []
	y_test = []
	for i in range(length-train_split_country):
		#add_x = [x_1[i+train_split_country],x_2[i+train_split_country]]
		add_x = [x_1[i+train_split_country],x_2[i+train_split_country],x_3[i+train_split_country]] # Performs better than the outcommented option above
		add_y = y[i+train_split_country]
		X_test.insert(i,add_x)
		y_test.insert(i,add_y)
		
	return X_train, y_train, X_test, y_test, X_full