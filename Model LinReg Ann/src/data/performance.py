from data import write_csv

def computeErrors(y_prediction_performance, y_measurment_target_day, forecast, forecast_delta, csv, check_csv_performance):
	
	error_absolute_D = [[[]for j in range(3)] for k in range(len(forecast))]
	error_absolute_I = [[[]for j in range(3)] for k in range(len(forecast))]
	error_MS_D = [[[]for j in range(3)] for k in range(len(forecast))]
	error_MS_I = [[[]for j in range(3)] for k in range(len(forecast))]
	
	for prediction_count in range(len(forecast)):
		predictions_D = y_prediction_performance[prediction_count][0][:]
		predictions_I = y_prediction_performance[prediction_count][2][:]
		measured_D = y_measurment_target_day[prediction_count][0][:]
		measured_I = y_measurment_target_day[prediction_count][2][:]
		
		error_absolute_D[prediction_count][0] = 0
		error_absolute_I[prediction_count][2] = 0
		error_MS_D[prediction_count][0] = 0
		error_MS_I[prediction_count][2] = 0
		epsilon = 0.0
		for num_measures in range(len(measured_D)):
			if measured_D[num_measures] == 0:
				epsilon = 1.0
			error_absolute_D[prediction_count][0] += abs(predictions_D[num_measures]-measured_D[num_measures])/(measured_D[num_measures]+epsilon)
			error_MS_D[prediction_count][0] += ((predictions_D[num_measures]-measured_D[num_measures])/(measured_D[num_measures]+epsilon)) ** 2

			if measured_I[num_measures] == 0:
				epsilon = 1
			error_absolute_I[prediction_count][2] += abs(predictions_I[num_measures]-measured_I[num_measures])/(measured_I[num_measures]+epsilon)
			error_MS_I[prediction_count][2] += ((predictions_I[num_measures]-measured_I[num_measures])/(measured_I[num_measures]+epsilon)) ** 2

		error_absolute_D[prediction_count][0] /= (len(predictions_D))
		error_absolute_I[prediction_count][2] /= (len(predictions_I))
		error_MS_D[prediction_count][0] /= (len(predictions_D))
		error_MS_I[prediction_count][2] /= (len(predictions_I))
		
		# Write to csv-file
		if csv == True:
			write_csv.performance(error_absolute_D[prediction_count][0], error_absolute_I[prediction_count][2], error_MS_D[prediction_count][0], error_MS_I[prediction_count][2], forecast[prediction_count], forecast_delta, check_csv_performance)
			check_csv_performance = 1
