import os, csv, datetime

def predictions(forecast_delta, days_predicted, check_csv_forecast, country, y_prediction_target_day):
	# create a directory to save the predictions
	today = datetime.date.today() - datetime.timedelta(days=forecast_delta)
	path = os.getcwd()
	path += '/' + today.isoformat()
	# check if the directory exists already and create it if not
	if not os.path.exists(path):
		try:
			os.mkdir(path)
		except OSError:
			print('Creation of the direcotry %s failed' %path)
		else: 
			print('Successfully created the directory %s' %path)					
	# save the forecast results in the newly created folder 
	with open(str(today.isoformat()) + '/' +str(days_predicted) + 'day_prediction.csv', 'a', newline='') as file:
		writer = csv.writer(file)
		if check_csv_forecast == 0:
			writer.writerow(['Province/State', 'Country', 'Target/Date','N','D'])
		date = datetime.date.today() + datetime.timedelta(days = int(days_predicted))
		date = date.isoformat()
		writer.writerow(['', country, date, y_prediction_target_day[2][-1], y_prediction_target_day[0][-1]])
	
def performance(error_absolute_D, error_absolute_I, error_MS_D, error_MS_I, days_predicted, forecast_delta, check_csv_performance):
	# create a directory to save performance results
	today_performance = datetime.date.today() - datetime.timedelta(days=forecast_delta)
	path_performance = os.getcwd()
	path_performance += '/ Performance on ' + today_performance.isoformat()
	# check if the directory exists already and create it if not
	if not os.path.exists(path_performance):
		try:
			os.mkdir(path_performance)
		except OSError:
			print('Creation of the direcotry %s failed' %path_performance)
		else: 
			print('Successfully created the directory %s' %path_performance)
	# save the performance results in the newly created folder 
	with open('Model_Performance.csv', 'a', newline='') as file:
		writer = csv.writer(file)
		if check_csv_performance == 0:
			writer.writerow(['Predicted days', 'Relative Error Infections', 'MSE Infections', 'Relative Error Deaths', 'MSE Deaths'])
		writer.writerow([days_predicted, error_absolute_I, error_MS_I, error_absolute_D, error_MS_D])