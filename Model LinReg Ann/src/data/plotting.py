import numpy as np
import matplotlib.pyplot as plt
import datetime

def plotCurves(X_full,days_predicted,y_predictions_ts,state_string,country,forecast_delta):
	today = datetime.date.today() - datetime.timedelta(days=forecast_delta)
	time_data = []
	time_measurments = len(X_full)
	for time in range(time_measurments):
		time_data.insert(time,time)
	time_prediction = []
	for time in range(time_measurments):
		time_prediction.insert(time+days_predicted,time+days_predicted)
	recovered = []
	dead = []
	infected = []
	for i in range(len(X_full)):
		recovered.insert(i,X_full[i][0])
		dead.insert(i,X_full[i][1])
		infected.insert(i,X_full[i][2])
		
	# preparing figure layout: size, axis-labels, colours, title, ...
	fig, ax1 = plt.subplots(figsize=(12,4))
	fig.suptitle(str(days_predicted) + '-day forecast for ' + country, fontsize='large',y=0.94)
	ax1.set_xlabel('Days since first case')
	ax1.set_ylabel('Total number of infections', color='red')
	ax1.tick_params(axis='y', labelcolor='red')
	ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
	ax2.set_ylabel('Total number of deaths', color='blue')
	ax2.tick_params(axis='y', labelcolor='blue')
	
	ax2.plot(time_data,dead, '-', color='blue')
	ax1.plot(time_data,infected, '-', color='red')
	ax1.plot(time_prediction,y_predictions_ts[2], '.', color='maroon', markersize=8)
	ax2.plot(time_prediction,y_predictions_ts[0], '.', color='darkcyan', markersize=8)
	
	
	#use the same tick-spacing for both y axis and make deaths-line smaller
	ax2.set_yticks(np.linspace(ax2.get_yticks()[0], int(ax2.get_yticks()[-1]*1.5), len(ax1.get_yticks())))
	
	# plotting start line
	ylim = ax2.get_ylim()
	start_idx = len(dead)-1
	ax2.plot((start_idx, start_idx), ylim, color='black')
	
	# adding legend, grid and layout type
	fig.legend(['Actual infected', 'Forecast infected', 'Actual dead', 'Forecast dead', 'Forecast Start: ' +str(today- datetime.timedelta(days=1))], 
			   loc=(0.092,0.55), fontsize='medium')
	fig.tight_layout()
	ax1.grid(True)

	plt.savefig(country + str(days_predicted) + '.png')
	plt.show()