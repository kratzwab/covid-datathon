from data import data_loader, LinReg, performance, write_csv, plotting
import numpy as np

if __name__ == "__main__":
	# get number of cases for R,D,I in all countries
	print("load data...")
	recovered_raw = data_loader.fetch_data_jhu(data_loader.source_recoverd)
	dead_raw = data_loader.fetch_data_jhu(data_loader.source_dead)
	infected_raw = data_loader.fetch_data_jhu(data_loader.source_infected)
	list_of_countries = dead_raw["Country/Region"].values #List of all countries
	
	plot = True # Set to True if you want to plot data & prediction
	plot_country = 'Switzerland' # Country of which you want to plot the data
	csv = True # Set to True if you want to write the prediction & performance indicators to the csv file
	check_csv_forecast = 0 # check variable
	check_csv_performance = 0
	test_train_split = 0.7 # portion of data for testing the model
	count_country = 0
	count_forecast = 0
	forecast = [2,7,30] # how many days to predict
	forecast_delta = 5; # number of days to subtract from the available data. this data-stock will be used to make the prediction
	# Stores the values of the most recent day as:
	# [ [ [list of most recent D for each country] [R] [I] ] 		2-days
	#   [ [list of most recent D for each country] [R] [I] ]		7-days
	#   [ [list of most recent D for each country] [R] [I] ]  ]		30-days
	y_prediction_target_day = [[[]for j in range(3)] for k in range(len(forecast))]
	y_prediction_performance = [[[]for j in range(3)] for k in range(len(forecast))]
	y_measurment_target_day = [[[]for j in range(3)] for k in range(len(forecast))]
	
	for days_predicted in forecast:
		for country in list_of_countries:
			print('Modelling Covid in', country)
			
			# list for country specific cases of R,D,I
			num_elements = len(recovered_raw.loc[recovered_raw["Country/Region"] == country].values[0][:])
			country_R = recovered_raw.loc[recovered_raw["Country/Region"] == country].values[0][1:num_elements-forecast_delta]	
			country_D = dead_raw.loc[dead_raw["Country/Region"] == country].values[0][1:num_elements-forecast_delta]
			country_I = infected_raw.loc[infected_raw["Country/Region"] == country].values[0][1:num_elements-forecast_delta]
			
			# the last entry of each list will always be the variable to be forecasted 
			state = [[country_I, country_R, country_D],
					 [country_D, country_I, country_R],
					 [country_R, country_D, country_I]]
			state_string = [['Infections','Recoveries','Deaths'],
				 			['Deaths','Infections','Recoveries'],
				 			['Recoveries','Deaths','Infections']]
				
			# timeseries of predictions of D,R,I for plotting (in this order, see list above)	
			y_predictions_ts = [[] for k in range(len(state))]
						
			for i in range(len(state)):
				y_measurment_target_day[count_forecast][i].insert(count_country,state[i][2][-1])

				# generate data for the linear regression model
				X_train, y_train, X_test, y_test, X_full = data_loader.generate_data(state[i], country, test_train_split, days_predicted, i) # devides the provided data into train and test data sets
				
				# use the data for the linear regression model
				# prediction for "forecast"-days after the last measurment, based on the model built from test&train data, applied to all the data in X_full
				y_prediction = LinReg.model(X_train, y_train, X_test, y_test, X_full)
				for k in range(len(y_prediction)):
					y_prediction[k] = int(y_prediction[k])
				y_prediction_target_day[count_forecast][i].insert(count_country,y_prediction[-1])
				y_prediction_performance[count_forecast][i].insert(count_country,y_prediction[len(y_prediction)-days_predicted])
				for j in range(len(y_prediction)):
					y_predictions_ts[i].insert(j,y_prediction[j])
			
			# Writ to csv-file
			if csv == True:
				write_csv.predictions(forecast_delta, days_predicted, check_csv_forecast, country, y_prediction_target_day[count_forecast])
				check_csv_forecast = 1				
				
			# Plot
			if plot == True:
				if country == plot_country:
					plotting.plotCurves(X_full,days_predicted,y_predictions_ts,state_string,country,forecast_delta)
				
			count_country += 1
		
		count_forecast += 1
		
	performance.computeErrors(y_prediction_performance, y_measurment_target_day, forecast, forecast_delta, csv, check_csv_performance)