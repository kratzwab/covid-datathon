#!/usr/bin/env python

import pandas as pd
from tqdm import tqdm
import numpy as np
from keras.preprocessing.sequence import TimeseriesGenerator


source_recoverd = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv"
source_infected = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv"
source_dead = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv"

def fetch_data_jhu(link):
	data = pd.read_csv(link)
	# for countries like australia or canada, wher JHU only provides numbers on regional level
	data = data.groupby(['Country/Region']).sum().reset_index()
	# drop unneccessary cols 
	data = data.drop(["Lat","Long"], axis=1)
	return data


def generate_time_series(raw_data, list_of_countries, time_window, train_test_split=1.0):
	# you can call this function with data returned by fetch_data_jhu from above, either infected, recovered, or dead
	X_train = []
	y_train = []
	X_test = []
	y_test = []
	infected, dead = raw_data
	for j,country in tqdm(enumerate(list_of_countries)): # create a time series for every country
		cols = infected.loc[infected["Country/Region"] == country].columns[2:-2]
		country_data = infected.loc[infected["Country/Region"] == country].values[0][2:-2]
		country_data_d = dead.loc[dead["Country/Region"] == country].values[0][2:-2]
		assert len(cols) == len(country_data) == len(country_data_d)
		assert (cols[-1] == "6/19/20")

		labels = [] # this is the countiry specific time series
		features = []
		first_zero = True  # skip the beginning of the time series where every country is only 0 and start at the first case
		for i in range(len(country_data)-1):
			if country_data[i] == 0 and first_zero:
				continue
			first_zero = False
			# I add the daily number of cases (or deaths) not the total number since I think 
			# the first one is easier to predict
			labels.append((country_data[i+1]-country_data[i],country_data_d[i+1]-country_data_d[i])) 
			feature = np.zeros((3))
			feature[0]=country_data[i]-country_data[i-1]
			feature[1]=country_data_d[i]-country_data_d[i-1]
			feature[2]=i%7
			features.append(feature)

		# now generate time series x,y data for the country and training data
		# basically it takes a winde of "time_window" days as x and uses the next day as y 

		train_generator = TimeseriesGenerator(features, labels, length=time_window, batch_size=1)     
		for i in range(len(train_generator)):
			x, y = train_generator[i]
			if i < len(train_generator)*train_test_split:
				X_train.append(x)
				y_train.append(y)
			else:
				X_test.append(x)
				y_test.append(y)



	y_train = np.vstack(y_train)
	#y_test = np.vstack(y_test)
	#X_test = np.vstack(X_test)
	X_train = np.vstack(X_train)

	return X_train, y_train, X_test, y_test
