import numpy as np


data = np.genfromtxt('2020-06-19/2day_prediction.csv',delimiter=',')

error_absolute_d = 0
error_absolute_n = 0
error_ms_d = 0
error_ms_n = 0

for row in data[1:]:
	if row[5] == 0: 
		if not row[3] == 0:
			error_absolute_n += 1
			error_ms_n += 1
	else:
		error_absolute_n += abs(row[3]-row[5])/row[5]
		error_ms_n += ((row[3]-row[5])/row[5]) ** 2

	if row[6] == 0: 
		if not row[4] == 0:
			error_absolute_d += 1
			error_ms_d += 1
	else:
		error_absolute_d += abs(row[4]-row[6])/row[6]
		error_ms_d += ((row[4]-row[6])/row[6]) ** 2


error_absolute_d /= (len(data)-1)
error_absolute_n /= (len(data)-1)
error_ms_d /= (len(data)-1)
error_ms_n /= (len(data)-1)

print("DEAD")
print("average mse per country = ",error_ms_d)
print("absolute error per country = ",error_absolute_d)
print("INFECTED")
print("average mse per country = ",error_ms_n)
print("absolute error per country = ",error_absolute_n)
