import datetime
import maptlotlib as plt
today = datetime.date.today().isoformat()

def plot_country_prediction(country_name, days_to_predict, ts_inf_actual, ts_inf_pred, ts_dead_actual, ts_dead_pred):
    """
    country_name: name of country, i.e. 'Switzerland'
    days_to_predict: amount of forecast days: i.e. 7 
    ts_inf_actual/ts_dead_actual: array or list of the timeseries of actual infections/deaths from day 0 to today from country_name
    ts_inf_pred/ts_dead_pred: array or list of timeseries of infections/deaths of trained model from day 0 to (today-days_to_predict) and forecast from this day on
    """
    # preparing figure layout: size, axis-labels, colours, title, ...
    fig, ax1 = plt.subplots(figsize=(12,4))
    fig.suptitle(str(days_to_predict) + '-day forecast for ' + country_name, fontsize='large')
    ax1.set_xlabel('Days since first case')
    ax1.set_ylabel('Total number of infections', color='red')
    ax1.tick_params(axis='y', labelcolor='red')
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.set_ylabel('Total number of deaths', color='blue')
    ax2.tick_params(axis='y', labelcolor='blue')

    # plotting infections
    ax1.plot(ts_inf_actual, '-', color='red')
    ax1.plot(ts_inf_pred, '.', color='maroon', markersize=8)
    # plotting deaths
    ax2.plot(ts_dead_actual, '-', color='blue')
    ax2.plot(ts_dead_pred, '.', color='darkcyan', markersize=8) 
    
    #use the same tick-spacing for both y axis and make deaths-line smaller
    ax2.set_yticks(np.linspace(ax2.get_yticks()[0], int(ax2.get_yticks()[-1]*1.5), len(ax1.get_yticks())))
    # plotting start line
    ylim = ax2.get_ylim()
    start_idx = len(ts_inf_actual) - days_to_predict
    ax2.plot((start_idx, start_idx), ylim, color='black')
    # adding legend, grid and layout type
    fig.legend(['Actual infected', 'Forecast infected', 'Actual dead', 'Forecast dead', 'Forecast Start: ' + today], 
               loc=(0.092,0.55), fontsize='medium')
    fig.tight_layout()
    ax1.grid(True)
    
    plt.savefig(country_name + str(days_to_predict) + '.png')
    plt.show()