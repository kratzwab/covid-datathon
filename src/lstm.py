from data import data_loader
import tensorflow as tf
from keras.models import Sequential
import keras.optimizers as optim
import datetime 
import os
import csv
import numpy as np
from keras.layers import LSTM, Dense, Dropout, LeakyReLU
from keras import backend as K
from keras.preprocessing.sequence import TimeseriesGenerator

time_window = 13 # look at past 20 days to make prediction for the next day


if __name__ == "__main__":
	# get number of infected in all countries
	print("load data...")
	infected = data_loader.fetch_data_jhu(data_loader.source_infected)
	dead = data_loader.fetch_data_jhu(data_loader.source_dead)

	# list of all countries
	list_of_countries = infected["Country/Region"].values	

	# generate time series data
	X_train, y_train, X_test, y_test = data_loader.generate_time_series((infected,dead), list_of_countries, time_window)
	print(np.shape(X_train))
	print(np.shape(X_test))
	# train a LSTM model
	model = Sequential()
	model.add(LSTM(64, return_sequences=True, recurrent_dropout=0.2, input_shape=(time_window,3)))
	model.add(LSTM(32, activation='relu', recurrent_dropout=0.2,))
	model.add(Dense(128))
	model.add(Dropout(0.3))
	model.add(Dense(2, activation=LeakyReLU(alpha=0.1)))
	opt = optim.Adam(learning_rate=0.0001)
	
	model.compile(loss=tf.keras.losses.MeanSquaredLogarithmicError(), optimizer=opt)
	model.fit(X_train, y_train, batch_size=64, epochs=12, 
		#validation_data=(X_test, y_test),
		 verbose=1, shuffle=True)

	model.save_weights('./tmp.model')

	pred = []
	solution = []

	for country in list_of_countries:
		print(country)
		model.load_weights('./tmp.model')

		cols = infected.loc[infected["Country/Region"] == country].columns[2:-2]
		assert (cols[-1] == "6/19/20")

		country_data = infected.loc[infected["Country/Region"] == country].values[0][2:-2]
		country_data_d = dead.loc[dead["Country/Region"] == country].values[0][2:-2]
		assert len(cols) == len(country_data) == len(country_data_d)

		labels = [] # this is the countiry specific time series
		features = []
		first_zero = True  # skip the beginning of the time series where every country is only 0 and start at the first case
		for i in range(len(country_data)-1):
			if country_data[i] == 0 and first_zero:
				continue
			first_zero = False
			# I add the daily number of cases (or deaths) not the total number since I think 
			# the first one is easier to predict
			labels.append(country_data[i+1]-country_data[i]) 
			feature = np.zeros((3))
			feature[0]=country_data[i]-country_data[i-1]
			feature[1]=country_data_d[i]-country_data_d[i-1]
			feature[2]=i%7
			features.append(feature)

		X_train = []
		y_train = []
		train_generator = TimeseriesGenerator(features, labels, length=time_window, batch_size=1)     
		for i in range(len(train_generator)):
			x, y = train_generator[i]
			X_train.append(x)
			y_train.append(y)
		y_train = np.vstack(y_train)
		X_train = np.vstack(X_train)
		K.set_value(model.optimizer.learning_rate, 0.00001)
		model.fit(X_train, y_train, batch_size=64, epochs=12, verbose=1, shuffle=True)
		
		x = np.zeros((1,time_window,3))
		x[0,:,:] = features[-time_window:]
		day2 = model.predict(x)
		day2 = day2[0]
		day2[0] = max(0,int(day2[0]))
		day2[1] = max(0,int(day2[1]))
		pred.append((country, day2[0], day2[1]))
		print(day2)

		cols = infected.loc[infected["Country/Region"] == country].columns[-1]
		assert (cols == "6/21/20")

		gtn = infected.loc[infected["Country/Region"] == country].values[0][-1]-infected.loc[infected["Country/Region"] == country].values[0][-2]
		gtd = dead.loc[dead["Country/Region"] == country].values[0][-1]-dead.loc[dead["Country/Region"] == country].values[0][-2]
		solution.append((gtn,gtd))
		#for (x,y) in zip(X_test, y_test):
		#	print(x)
		#	print(model.predict(np.expand_dims(x,0)))
		#	 print(y)

	# first, we create a directory to save our predictions
	today = datetime.date.today() - datetime.timedelta(days = 3)
	path = os.getcwd()
	path += '/' + today.isoformat()

	# let's check if the directory excists already and create it if not
	if not os.path.exists(path):
	    try:
	        os.mkdir(path)
	    except OSError:
	        print('Creation of the direcotry %s failed' %path)
	    else: 
	        print('Successfully created the directory %s' %path)
	        
	# now we can save the forecast results in the newly created folder  
	with open(today.isoformat() + '/' + '2day_prediction.csv', 'w', newline='') as file:
	    writer = csv.writer(file)
	    writer.writerow(['Province/State','Country', 'Target/Date','N','D','GTN', 'GTD'])
	    for p,s in zip(pred,solution):
	        date = datetime.date.today() + datetime.timedelta(days = 2)
	        date = date.isoformat()
	        writer.writerow(['', p[0], date, p[1], p[2], s[0], s[1]])