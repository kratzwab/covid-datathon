import numpy as np
from scipy.integrate import solve_ivp
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import csv
import random


####  CH data for tests ######

population_ch = 8570000
zero = np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
capA = len(zero)
confirmed_ch = np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,8,8,18,27,42,56,90,114,214,268,337,374,491,652,652,1139,1359,2200,2200,2700,3028,4075,5294,6575,7474,8795,9877,10897,11811,12928,14076,14829,15922,16605,17768,18827,19606,20505,21100,21657,22253,23280,24051,24551,25107,25415,25688,25936,26336,26732,27078,27404,27740,27944,28063,28268,28496,28677,28894,29061,29164,29264,29407,29586,29705,29817,29905,29981,30009,30060,30126,30207,30251,30305,30344,30380,30413,30463,30514,30572,30587,30597,30618,30658,30694,30707,30725,30736,30746,30761,30776,30796,30828,30845,30862,30871,30874,30893,30913,30936,30956,30965,30972,30988,31011,31044,31063,31094])
recovered_ch = np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,3,3,3,3,3,3,3,4,4,4,4,4,4,4,15,15,15,15,131,131,131,131,131,1530,1530,1595,1823,1823,2967,4013,4846,6415,6415,8056,8704,9800,10600,11100,12100,12700,13700,13700,15400,15900,16400,17100,17800,18600,19400,19900,20600,21000,21300,21800,22200,22600,22600,23400,23900,24200,24500,25200,25400,25700,25900,26100,26400,26600,26800,26800,27100,27100,27100,27400,27500,27600,27700,27800,27900,27900,28000,28100,28200,28200,28300,28300,28300,28400,28500,28500,28500,28600,28600,28600,28700,28700,28700,28700,28700,28800,28800,28800])
death_ch = np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,2,2,3,4,4,11,13,14,14,27,28,41,54,75,98,120,122,153,191,231,264,300,359,433,488,536,591,666,715,765,821,895,948,1002,1036,1106,1138,1174,1239,1281,1327,1368,1393,1429,1478,1509,1549,1589,1599,1610,1665,1699,1716,1737,1754,1762,1762,1784,1795,1805,1810,1823,1830,1833,1845,1867,1870,1872,1878,1879,1881,1886,1891,1892,1898,1903,1905,1906,1913,1915,1917,1919,1919,1919,1920,1920,1920,1921,1921,1921,1921,1921,1923,1934,1936,1937,1938,1938])
print('time: ', len(confirmed_ch))


I = (confirmed_ch-recovered_ch-death_ch)[capA:]
print(I)
R = recovered_ch[capA:]
D = death_ch[capA:]
#plt.plot(time,population_ch-confirmed_ch-recovered_ch-death_ch,label='S')

time = np.arange(0,len(I),1)


plt.plot(time,I,label='I',color='y')
plt.plot(time,R,label='R',color='g')
plt.plot(time,D,label='D',color='r')
plt.title('Data CH')
plt.legend()
plt.figure()

###### End CH Data #####

#### SIRD Parameters ######

Beta = 0.5 #rate of infection
Gamma = 0.1 # rate of recovery
Mu = 0.05 # rate of mortality
N = population_ch # popultoin size

A0 = np.array([N-1,1,0,0]) #start values [S0,I0,R0,D0]
####################

####  implmentation of the side model ###

def rhs(t,A):
    """
        Right hand side of the SIRD model:
        
        -       -       -                       -
        | ds/dt |       | - Beta * I * S / N    |
        | dI/dt | =     |   Beta * I * S / N    |
        | dR/dt |       |   Gamma * I           |
        | dD/dt |       |   Mu * I              |
        -       -       -                       -
    """
    #print('the params are now:' ,np.array([Beta,Gamma,Mu]))
    return np.array([  -Beta*A[1]*A[0]/N,
                        Beta*A[1]*A[0]/N - Gamma * A[1] - Mu * A[1],
                        Gamma*A[1],
                        Mu*A[1]     ])

####################

####### Loss function for fitting #####
def f_optimize(x0):
    print('got called')
    global Beta
    Beta = x0[0]
    global Gamma
    Gamma = x0[1]
    global Mu
    Mu = x0[2]
    print('Param: ',np.array([Beta,Gamma,Mu]))
    res = solve_ivp(rhs,np.array([0,144]),A0,max_step=1,method='BDF')
    return np.sum(np.array([np.sum(I-res.y[1][:144])**2 , np.sum(R -res.y[2][:144])**2 , np.sum(D-res.y[3][:144])**2 ]))
################

######## Magical fitting that does not work ############



########################################################

############# Create training set #####
"""
o = open('train.txt','w+')
for i in range(1000):
#    global Beta
    Beta = random.random()
 #   global Gamma 
    Gamma = random.random()/10
  #  global Mu
    Mu = random.random()/100
    res = solve_ivp(rhs,np.array([0,len(I)]),A0,max_step=1,method='BDF') #Nummericaly solve the system of OEDs
    wr = str(Beta) + ',' + str(Gamma) + ',' + str(Mu) + ','
    for k in res.y[1][:130]:
        wr += str(np.round(k,3)) + ','
    o.writelines(wr[:-1]+ str('\n'))
"""
########  Plotting ##############

res = solve_ivp(rhs,np.array([0,len(I)]),A0,max_step=1,method='BDF') #Nummericaly solve the system of OEDs

#plt.plot(res.t,res.y[0],label='S')
plt.plot(time,I,label='I CH',color='y',linestyle=':')
plt.plot(time,R,label='R CH',color='g',linestyle=':')
plt.plot(time,D,label='D CH',color='r',linestyle=':')

plt.plot(res.t,res.y[1],label='I',color = 'y')
plt.plot(res.t,res.y[2],label='R',color = 'g')
plt.plot(res.t,res.y[3],label='D',color = 'r')
plt.title(r'SIRD Model Beta = '+ str(Beta)+ ' Gamma = '+ str(Gamma) + ' Mu = ' + str(Mu))
plt.legend()
plt.show()
